import { Resolver } from '@nestjs/graphql';
import { Comment } from './comments.type';

@Resolver(() => Comment)
export class CommentsResolver {}
