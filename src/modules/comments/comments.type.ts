import { Field, Int, ObjectType } from '@nestjs/graphql';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Car } from '../cars/car.type';

@ObjectType()
@Entity('comments')
export class Comment {
  @Field(() => Int)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  text: string;

  @Field(() => Int)
  @Column({ name: 'car_id' })
  carId: number;

  @Field()
  @ManyToOne(() => Car)
  @JoinColumn({ name: 'car_id' })
  car: Car;
}
