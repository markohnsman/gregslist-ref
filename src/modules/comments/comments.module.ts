import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Comment } from './comments.type';
import { CommentsResolver } from './comments.resolver';
import { CommentService } from './comments.service';

@Module({
  imports: [TypeOrmModule.forFeature([Comment])],
  providers: [CommentsResolver, CommentService],
  exports: [CommentService],
})
export class CommentsModule {}
