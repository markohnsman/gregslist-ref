import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Comment } from './comments.type';

@Injectable()
export class CommentService {
  constructor(
    @InjectRepository(Comment)
    private repository: Repository<Comment>,
  ) {}

  async getComments(carId: number): Promise<Comment[]> {
    return await this.repository.find({ where: { carId } });
  }
}
