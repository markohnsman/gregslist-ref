import { Inject } from '@nestjs/common';
import { CarService } from './car.service';
import { Car } from './car.type';
import {
  Args,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { CreateCarInput } from './createCar.input';
import { CommentService } from '../comments/comments.service';
import { Comment } from '../comments/comments.type';

@Resolver(() => Car)
export class CarResolver {
  constructor(
    @Inject(CarService)
    private readonly carService: CarService,

    @Inject(CommentService)
    private readonly commentService: CommentService,
  ) {}

  @ResolveField(() => [Comment], { nullable: true })
  async comments(@Parent() car: Car): Promise<Comment[]> {
    return await this.commentService.getComments(car.id);
  }

  @Query(() => [Car], { nullable: true })
  async getCars() {
    return await this.carService.getCars();
  }

  @Query(() => Car, { nullable: true })
  async getCarById(@Args('id') id: number) {
    return await this.carService.getCarById(id);
  }

  @Mutation(() => Car)
  async createCar(
    @Args('createCarInput') createCarInput: CreateCarInput,
  ): Promise<Car> {
    return await this.carService.createCar(createCarInput);
  }
}
