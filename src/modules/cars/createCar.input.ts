import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class CreateCarInput {
  @Field()
  make: string;
  @Field()
  model: string;
  @Field()
  year: string;
  @Field()
  price: number;
  @Field({ nullable: true })
  imgUrl?: string;
}
