import { Module } from '@nestjs/common';
import { CarResolver } from './car.resolver';
import { CarService } from './car.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Car } from './car.type';
import { CommentService } from '../comments/comments.service';
import { Comment } from '../comments/comments.type';

@Module({
  imports: [TypeOrmModule.forFeature([Car, Comment])],
  providers: [CarResolver, CarService, CommentService],
})
export class CarsModule {}
