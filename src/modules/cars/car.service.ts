import { Injectable } from '@nestjs/common';
import { Car } from './car.type';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCarInput } from './createCar.input';

@Injectable()
export class CarService {
  constructor(
    @InjectRepository(Car)
    private repository: Repository<Car>,
  ) {}
  async getCars(): Promise<Car[]> {
    return await this.repository.find();
  }

  async getCarById(id: number): Promise<Car> {
    return await this.repository.findOneBy({ id });
  }

  async createCar(createCarInput: CreateCarInput): Promise<Car> {
    const car = this.repository.create(createCarInput);
    return await this.repository.save(car);
  }
}
