import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { CarsModule } from './modules/cars/car.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Car } from './modules/cars/car.type';
import { Comment } from './modules/comments/comments.type';
import { CommentsModule } from './modules/comments/comments.module';

@Module({
  imports: [
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: 'src/graphql/gen/schema.gql',
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5434,
      username: 'postgres',
      password: 'docker',
      database: 'bcw_demo',
      entities: [Car, Comment],
      synchronize: true,
    }),
    CarsModule,
    CommentsModule,
  ],
  providers: [],
})
export class AppModule {}
