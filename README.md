# Gregslist Ref

This is a basic example app that demonstrates the usage of GraphQL, Nest, and TypeORM.

## Installation

1. Clone the repository:

  ```bash
  git clone https://github.com/your-username/gregslist-ref.git
  ```

2. Install the dependencies:

  ```bash
  cd gregslist-ref
  npm install
  ```

3. Set up the database:

  - Create a PostgreSQL database.
  - Update the database configuration in `src/config/database.config.ts` with your database credentials.

4. Start the application:

  ```bash
  npm run start:dev
  ```

## Usage

Once the application is running, you can access the GraphQL playground at `http://localhost:3000/graphql`. Here, you can interact with the GraphQL API and perform various operations.

## Contributing

Contributions are welcome! If you find any issues or have suggestions for improvements, please open an issue or submit a pull request.

## License

This project is licensed under the [MIT License](LICENSE).
